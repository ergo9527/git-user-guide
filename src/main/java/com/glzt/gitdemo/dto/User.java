package com.glzt.gitdemo.dto;

import lombok.Data;

@Data
public class User {

    private String name;
    private Long uid;
}
