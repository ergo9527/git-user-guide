package com.glzt.gitdemo.controller;

import com.glzt.gitdemo.dto.User;
import com.glzt.gitdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/get/{id}")
    public User findUserById(@PathVariable("id") Long id){
        return userService.findUserById(id);
    }
}
