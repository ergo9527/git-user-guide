package com.glzt.gitdemo.service;

import com.glzt.gitdemo.dto.User;
import org.springframework.stereotype.Service;


public interface UserService {
    User findUserById(Long id);

    void addUser(User user);

    void deleteUser(Long id);
}
