package com.glzt.gitdemo.service.impl;

import com.glzt.gitdemo.dto.User;
import com.glzt.gitdemo.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public User findUserById(Long id) {
        User user = new User();
        user.setUid(id);
        user.setName("test_user_01");
        return user;
    }

    @Override
    public void addUser(User user) {
        //A编写addUser()方法体
        //
    }

    @Override
    public void deleteUser(Long id) {

    }

}
